use std::sync::mpsc;

const DIGITS: &str = "0123456789";
const ALLOWED_CHARS: &str = "+-/*abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

fn main() {
    let code = "(Atan2
                    (SiN ( - 123.4 x)
                    (/ x y)))";
    let (sender, receiver) = mpsc::channel();

    // run a lexer concurrently with a parser...(which we do not have here)
    std::thread::spawn(move || {
        Lexer::begin_lexing(&code, sender);
    });

    while let Ok(token) = receiver.recv() {
        println!("Token received from channel: {token:?}");
    }
}

// tokens also contain the line number they occured on
#[derive(Debug)]
enum Token<'a> {
    OpenParen(usize),
    CloseParen(usize),
    Constant(&'a str, usize),
    // everything else is 'operation'
    Operation(&'a str, usize),
}

// StateFunction represents the state of the scanner, as a function that returns the next state
// Function pointer definition must be wrapped in a struct to be recursive
struct StateFunction(fn(&mut Lexer) -> Option<StateFunction>);

struct Lexer<'a> {
    // the string being scanned
    input: &'a str,
    // start position of this item
    start: usize,
    // current position of the input
    pos: usize,
    // width of the last char read
    width: usize,
    token_sender: mpsc::Sender<Token<'a>>,
    current_line: usize,
}

impl<'a> Lexer<'a> {
    fn begin_lexing(s: &'a str, sender: mpsc::Sender<Token<'a>>) {
        let mut lexer = Lexer::<'a> {
            input: s,
            start: 0,
            pos: 0,
            width: 0,
            token_sender: sender,
            current_line: 0,
        };
        lexer.run();
    }

    // no switch statement needed
    fn run(&mut self) {
        let mut state = Some(StateFunction(Lexer::determine_token));
        while let Some(next_state) = state {
            state = next_state.0(self)
        }
    }

    fn determine_token(l: &mut Lexer) -> Option<StateFunction> {
        while let Some(c) = l.next() {
            if Lexer::is_whitespace(c) {
                l.ignore();
            } else if c == '(' {
                l.emit(Token::OpenParen(l.current_line));
            } else if c == ')' {
                l.emit(Token::CloseParen(l.current_line));
            } else if Lexer::is_start_of_number(c) {
                return Some(StateFunction(Lexer::lex_number));
            } else {
                return Some(StateFunction(Lexer::lex_operation));
            }
        }
        None
    }

    fn next(&mut self) -> Option<char> {
        if self.pos >= self.input.len() {
            self.width = 0;
            None
        } else {
            let c = self.input[self.pos..].chars().next().unwrap();
            self.width = c.len_utf8();
            if Lexer::is_linebreak(c) {
                self.current_line += 1;
            }
            self.pos += self.width;
            Some(c)
        }
    }

    fn lex_number(l: &mut Lexer) -> Option<StateFunction> {
        l.accept("-");
        l.accept_run(DIGITS);
        if l.accept(".") {
            l.accept_run(DIGITS);
        }
        l.emit(Token::Constant(&l.input[l.start..l.pos], l.current_line));
        Some(StateFunction(Lexer::determine_token))
    }

    fn lex_operation(l: &mut Lexer) -> Option<StateFunction> {
        l.accept_run(ALLOWED_CHARS);
        l.emit(Token::Operation(&l.input[l.start..l.pos], l.current_line));
        Some(StateFunction(Lexer::determine_token))
    }

    fn emit(&mut self, token: Token<'a>) {
        self.token_sender
            .send(token)
            .expect("Send token on channel");
        self.start = self.pos;
    }

    // accept consumes the next char if it's from the valid set
    fn accept(&mut self, valid: &str) -> bool {
        match self.next() {
            Some(n) if valid.contains(n) => true,
            _ => {
                self.backup();
                false
            }
        }
    }

    // accept_run consumes a run of chars from the valid set
    fn accept_run(&mut self, valid: &str) {
        while let Some(n) = self.next() {
            if !valid.contains(n) {
                break;
            }
        }
        self.backup();
    }

    fn ignore(&mut self) {
        self.start = self.pos;
    }

    fn backup(&mut self) {
        self.pos -= self.width;
    }

    fn is_linebreak(c: char) -> bool {
        c == '\n'
    }

    fn is_whitespace(c: char) -> bool {
        c == ' ' || c == '\n' || c == '\t' || c == '\r'
    }

    fn is_start_of_number(c: char) -> bool {
        ('0' <= c && c <= '9') || c == '-' || c == '.'
    }
}
